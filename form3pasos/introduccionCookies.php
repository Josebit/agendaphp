<?php
//para mostrar errores
error_reporting(E_ALL);
ini_set('display_errors', 1);

?>

	
<?php
//http://es.wikieducator.org/Usuario:Lmorillas/desarrollo_web_servidor/php/cookies
function creaCookie($nombre){
	// CREAR COOKIE
	$expire=time()+60*60*24*30;
	//time() es ahora + 1min(60") * 1h(60') * 1dia(24h) * 30dias
	// (osea durará esta cookie a partir de ahora, 1 mes)
	
	setcookie("micookie", $nombre, $expire);
}

function borraCookie(){
	// BORRAR UNA COOKIE
	// set the expiration date to one hour ago
	setcookie("micookie", "", time()-3600);
	// consiste en darle un valor en blanco y un tiempo de expiración ya pasado
}
?>



<?php
if (isset($_COOKIE["micookie"]))
  echo "Hola " . $_COOKIE["micookie"] . "!<br />";
else
  echo "Hola anonimo!<br />";
?>	

<!-- FORMULARIO COOKIE  -->
    <form action="introduccionCookies.php" method="post" class="form-horizontal" role="form">
	  <div class="form-group">
	    <label for="nombre" class="col-sm-2 control-label">introduce tu nombre</label>
	    <div class="col-xs-4">
	      <input type="text" class="form-control" name="nombre" id="nombre" placeholder="nombre">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">Crear Cookie con tu nombre</button>
	      <a href="index.php">
	      	<button type="button" class="btn btn-default" >Cancelar</button>
      	  </a>
	    </div>
	  </div>
	</form>



<?php
if (isset($_POST['nombre'])){
	$nombre = $_POST['nombre'];
	creaCookie($nombre);
}
?>
	



