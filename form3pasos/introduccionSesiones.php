<?php
//para mostrar errores
error_reporting(E_ALL);
ini_set('display_errors', 1);

?>

<?php 
// http://localhost:8888/workspace-aptana/agendaPHP/form3pasos/introduccionSesiones.php

//antes de enviar html al cliente
session_start(); //siempre que usemos sesiones 
/*
 * La sesion muere al hacer un logout o al salir del navegador.
 * Las sesiones se guardan en el servidor
 * En el cliente solo se guarda una referencia de sesion en cookie PHPSESSID
 * Una cookie vive lo que le digamos en expire.
 * 
 */

if (!isset($_SESSION["cuenta_paginas"])){
    $_SESSION["cuenta_paginas"] = 1;	//clave = valor
}else{
    $_SESSION["cuenta_paginas"]++;
}

if (!isset($_SESSION['contador'])){
	$_SESSION['contador']=1;
}else{
	$_SESSION["contador"]++;
}



//VARIABLES GLOBALES
var_dump($_COOKIE);
echo "<br>";
var_dump($_SESSION);
echo "<br>";
var_dump($_REQUEST);
echo "<br>";

//observa que si comentas esta linea siempre será el mismo identificador de sesion
session_regenerate_id();  // PARA REGENERAR EL IDENTIFICADOR GUARDADO EN LA COOKIE DEL CLIENTE
//MUY IMPORTANTE
?>

<?php
echo "<p>Cada vez que refresques se actualiza el valor de sesion</p>";
echo "<p>Desde que entraste has visto ".$_SESSION["cuenta_paginas"]." paginas</p>";

echo "<p>Contador dice: ".$_SESSION["contador"]." </p>";



?>
  
 
	
	
	