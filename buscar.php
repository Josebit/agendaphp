<?php 
include "cabecera.php";
?>
  <body>

    <div class="container">

	      <!-- Menu -->
		  <?php 
			include "menu.php";
		  ?>
	      
	      <!-- Main component for a primary marketing message or call to action -->
	      <div class="jumbotron">
	        <h2>Buscar contacto</h2>
	      </div>
		
	      <!-- Formulario buscar -->
		  <?php 
			include "buscar_form.php";
		  ?>
		  

    </div> <!-- /container -->


<!-- Pie (Bootstrap core JavaScript) -->
<?php 
include "pie.php";
?>