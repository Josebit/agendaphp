
      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Agenda PHP</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Inicio</a></li> <!-- class="active" -->
            <li><a href="listado.php">Listado</a></li>
            <li><a href="añadir.php">Añadir</a></li>
            <li><a href="buscar.php">Buscar</a></li>
          </ul>
		
          <ul class="nav navbar-nav navbar-right">
            <li><a href="crearBD.php">crear BD inicial</a></li>
            <li><a href="login.php">login</a></li>
            <li><a href="#">logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>

      