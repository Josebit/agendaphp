<?php
//para mostrar errores
error_reporting(E_ALL);
ini_set('display_errors', 1);

?>



<?php
//DAR PERMISOS A LA CARPETA Y CONTENIDO !!!!!!!

// variables
// TABLA CONTACTOS ---------------------------------------------------------------------
$crear_tabla = 'CREATE TABLE IF NOT EXISTS contactos(
												id INTEGER PRIMARY KEY AUTOINCREMENT,
											    nombre VARCHAR(40), 
											    apellidos VARCHAR(60), 
											    telefono VARCHAR(10),
											    correo VARCHAR(50)
												)';

$datosiniciales = array(
			//registro 1 inicial
			array(	
					'nombre'=>"Jose",
					'apellidos'=>"Lahuerta Jimeno",
					'telefono'=>"976 22 33 44",
					'correo'=>"jose@jose.com"
				),
			//registro 2 inicial
			array(	
					'nombre'=>"Maria",
					'apellidos'=>"Felices Campos",
					'telefono'=>"976 11 11 11",
					'correo'=>"maria@maria.com"
				),
			//registro 3 inicial
			array(	
					'nombre'=>"Bea",
					'apellidos'=>"Nutridas Cascojas",
					'telefono'=>"976 33 33 33",
					'correo'=>"bea@bea.es"
				)
		);




// TABLA USUARIOS ------------------------------------------------------------------
$crear_tabla_usuarios = 'CREATE TABLE IF NOT EXISTS usuarios(
												id INTEGER PRIMARY KEY AUTOINCREMENT,
											    nombre VARCHAR(40),
											    password VARCHAR(256)
												)';

$datosiniciales_usuarios = array(
		//registro 1 inicial
		array(
				'nombre'=>"jose",
				'password'=>"jose"
		),
		//registro 2 inicial
		array(
				'nombre'=>"maria",
				'password'=>"maria"
		)
);

?>


<?php
// si bd existe, no hagas nada
$nombre_fichero = 'agenda.sqlite';
if (file_exists($nombre_fichero)) {
    echo "<h3>El fichero $nombre_fichero ya existe</h3><br>";
	//echo '<a href="./index.php">Volver</a>';
} else {	
	// y si no crearla y rellenar unos datos iniciales
	try {
		// crear/conectar a bases de datos
		$conn = new PDO('sqlite:agenda.sqlite');
		//$conn = new PDO('sqlite::memory:'); //también se puede crear en memoria
		
		// creación de la tabla contactos ----------------------------------------------
		$conn->exec($crear_tabla);
		// insertar datos
		// 1. preparar sentencia de inserción
		$insertar = "INSERT INTO contactos(nombre, apellidos, telefono, correo)
					VALUES(:nombre, :apellidos, :telefono, :correo)";
		$sentencia = $conn->prepare($insertar);
	
		echo "<h3>Datos Insertados</h3>";
		
		foreach($datosiniciales as $contacto){
			$sentencia->execute($contacto);
			echo "Insertado ", $contacto['nombre'], '<br>';
			echo "Insertado ", $contacto['apellidos'], '<br>';
			echo "Insertado ", $contacto['telefono'], '<br>';
			echo "Insertado ", $contacto['correo'], '<br><hr>';
		}
		// select
		$listado = 'SELECT * FROM contactos';
		$resultado = $conn->query($listado);
	
		echo "<h3>Listado</h3>";
		
		foreach($resultado as $contacto){
			echo 'id: <strong>', $contacto['id'], '</strong><br>';
			echo 'nombre: <strong>', $contacto['nombre'], '</strong><br>';
			echo 'apellidos: <strong>', $contacto['apellidos'], '</strong><br>';
			echo 'telefono: <strong>', $contacto['telefono'], '</strong><br>';
			echo 'correo: <strong>', $contacto['correo'], '</strong><br><hr>';
		}
		// fin creación de la tabla contactos -------------------------------------------
		
		

		// creación de la tabla usuarios ----------------------------------------------
		// crear/conectar a bases de datos
		$conn2 = new PDO('sqlite:agenda.sqlite');
		$conn2->exec($crear_tabla_usuarios);
		// insertar datos
		// 1. preparar sentencia de inserción
		$insertar_usuarios = "INSERT INTO usuarios(nombre, password)
					VALUES(:nombre, :password)";
		$sentencia_usuarios = $conn2->prepare($insertar_usuarios);
		
		foreach($datosiniciales_usuarios as $user){
			$sentencia_usuarios->execute($user);
			//comprobación
			echo "Insertado ", $user['nombre'], '<br>';
			echo "Insertado ", $user['password'], '<br>';
		}
		// fin creacion tabla usuarios  ------------------------------------------------
		
			
	} //fin try
	catch(PDOException $e){
		echo $e->getMessage();
	} //fin catch
	
	// cierra conexion
	$conn = null;
	$conn2 = null;


echo "<h3>El fichero '".$nombre_fichero."' ha sido creado con exito.</h3><br>";	  //el punto concatena (si pongo comillas dobles)
echo '<a href="./index.php">Inicio</a>';
} //fin else
?>




