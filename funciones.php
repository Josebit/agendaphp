<?php 
class Usuario{
	//Atributos
	var $nombre;
	var $password;

	//constructor
	function __construct($nombre="", $password="")
	{
		$this->nombre = $nombre;
		$this->password = $password;
	}
	
	//metodos
	function compruebaUsuario(){
		/*
		 * devuelve true si usuario y contraseña ok en BD
		 * 
		 */		
		if ($this->nombre !="" && $this->password !="") {	// si hay datos en el formulario
			try{
				// crear/conectar a bases de datos
				$conn = new PDO('sqlite:agenda.sqlite');
		
				// mostrar coincidencias
				//$listado = "SELECT * FROM usuarios WHERE nombre=',$this->nombre,' AND password='$this->password'";
				$listado = "SELECT nombre, password FROM usuarios";
				$resultado = $conn->query($listado);
				
				//comprobación OK
				//echo 'OBJETO USUARIO DICE: mi nombre es: <strong>', $this->nombre, '</strong><br>';
				//echo 'OBJETO USUARIO DICE: mi password es: <strong>', $this->password, '</strong><br>';
				
				$valido = false;
				foreach($resultado as $row){					
// 					echo 'Nombre: <strong>', $row['nombre'], '</strong><br>';
// 					echo 'Password: <strong>', $row['password'], '</strong><br>';
// 					echo '<hr>';
					
					//compruebo que los atributos del objeto coinciden con la BD
					if ($this->nombre == $row['nombre'] && $this->password == $row['password']) {
						$valido = true;	
						break;					
					}else {
						$valido = false;
					}
				}
				return $valido;
		
			} //fin try
			catch(PDOException $e){
				echo $e->getMessage();
			} //fin catch
		
			// cierra conexion
			$conn = null;
		}
		//fin if (si se han introducido datos en el formulario)
	}
 	//fin function compruebaUsuario

	function creaSesion(){
		session_start(); // ESTO SIEMPRE LO PRIMERO SI TRABAJAMOS CON SESIONES
		// LA SESION SE GUARDA EN EL SERVIDOR
		// EL IDENTIFICADOR SE GUARDA EN UNA COOKIE EN EL CLIENTE
		
		if (isset($_SESSION[$this->nombre])){
			//echo "Hola '" . $_SESSION["usuario"] . "', bienvenido a la AgendaPHP<br />";
		}
		
	}
	//fin creaSesion()
	
	
}
//fin class Usuario







class Contacto{
	//Atributos
	var $id;	// el id llega por get siempre (no se construye)
	var $nombre;
	var $apellidos;
	var $telefono;
	var $correo;
	
	//constructor
 	function __construct($nombre="", $apellidos="", $telefono="", $correo="")
 	{
 		$this->nombre = $nombre;
 		$this->apellidos = $apellidos;
 		$this->telefono = $telefono;
 		$this->correo = $correo;
 	}
	
	//metodos
	function dameContacto($id){
		
		try {
			// crear/conectar a bases de datos
			$conn = new PDO('sqlite:agenda.sqlite');
			$sentencia = "SELECT * FROM contactos WHERE id =".$id;
			$contacto = $conn->query($sentencia);
			//para asegurarnos que solo hay un contacto
			foreach($contacto as $con)
			{
				//obtengo los atributos del objeto
				$this->id = $con['id'];
				$this->nombre = $con['nombre'];
				$this->apellidos = $con['apellidos'];
				$this->telefono = $con['telefono'];
				$this->correo = $con['correo'];
				break;
			}
		} //fin try
		catch(PDOException $e){
			echo $e->getMessage();
		} //fin catch
	
		// cierra conexion
		$conn = null;
	}
	//fin function dameContacto
	
	
	
	function eliminaContacto(){

// 		//comprobación OK
// 		echo "eliminaContacto() dice: Me ha llegado Nombre: ", $this->nombre, "<br>";  		// OK
// 		echo "eliminaContacto() dice: Me ha llegado Apellidos: ", $this->apellidos, "<br>"; // OK
// 		echo "eliminaContacto() dice: Me ha llegado Telefono: ", $this->telefono, "<br>";  	// OK
// 		echo "eliminaContacto() dice: Me ha llegado Correo: ", $this->correo, "<br>";  		// OK
		
		if ($this->nombre !="" && $this->apellidos != "" && $this->telefono != "" && $this->correo != "") {	// si hay datos en el formulario
			try{
				// crear/conectar a bases de datos
				$conn = new PDO('sqlite:agenda.sqlite');
		
				// preparar sentencia
				$sentencia = $conn->prepare("DELETE FROM contactos WHERE id=:id");
				$sentencia->bindParam(':id', $id);
		
				// eliminar una fila
				$id = $this->id;
				$sentencia->execute();	//ejecutar
				
			} //fin try
			catch(PDOException $e){
				echo $e->getMessage();
			} //fin catch
		
			// cierra conexion
			$conn = null;
		}
		//fin if (si se han introducido datos en el formulario)
			
		
		//redirijo a listado
		//redirijo a listado (ha de estar en archivo aparte por el header location)
		include 'redirige_a_listado.php';
			
	}
	//fin function eliminaContacto
	
	
	
	function añadeContacto(){

// 		//comprobación OK
// 		echo "añadeContacto() dice: Me ha llegado Nombre: ", $this->nombre, "<br>";  		// OK
// 		echo "añadeContacto() dice: Me ha llegado Apellidos: ", $this->apellidos, "<br>";  	// OK
// 		echo "añadeContacto() dice: Me ha llegado Telefono: ", $this->telefono, "<br>";  	// OK
// 		echo "añadeContacto() dice: Me ha llegado Correo: ", $this->correo, "<br>";  		// OK
		
		if ($this->nombre !="" && $this->apellidos != "" && $this->telefono != "" && $this->correo != "") {	// si hay datos en el formulario
			try{
				// crear/conectar a bases de datos
				$conn = new PDO('sqlite:agenda.sqlite');
				
				// preparar sentencia
				$sentencia = $conn->prepare("INSERT INTO contactos(nombre, apellidos, telefono, correo) VALUES(:nombre, :apellidos, :telefono, :correo)");
				$sentencia->bindParam(':nombre', $nombre);
				$sentencia->bindParam(':apellidos', $apellidos);
				$sentencia->bindParam(':telefono', $telefono);
				$sentencia->bindParam(':correo', $correo);
				
				// insertar una fila
				$nombre = $this->nombre;
				$apellidos = $this->apellidos;
				$telefono = $this->telefono;
				$correo = $this->correo;
				$sentencia->execute();	//ejecutar
				
				// GRACIAS A PREPARE() PODRIA insertar otra fila con diferentes valores
// 				$nombre = 'PEPE';
// 				$apellidos = 'LASGORGAS';
// 				$telefono = '976112233';
// 				$correo = 'PEPE@PEPE.COM';
// 				$sentencia->execute();	//ejecutar

			} //fin try
			catch(PDOException $e){
				echo $e->getMessage();
			} //fin catch
		
			// cierra conexion
			$conn = null;
			
		}
		//fin if (si se han introducido datos en el formulario)
			
		
		//redirijo a listado (ha de estar en archivo aparte por el header location)
		//include 'redirige_a_listado.php';
		header("Location: listado.php");
		exit;
	}
	//fin function añadeContacto
	

	
	
	function pintaFormEditar(){	//pinta un formulario relleno y submit a 'editar_accion.php'

// 		//comprobación OK
// 		echo "pintaFormEditar() dice: Me ha llegado id: ", $this->id, "<br>";  					// OK
// 		echo "pintaFormEditar() dice: Me ha llegado Nombre: ", $this->nombre, "<br>";  			// OK
// 		echo "pintaFormEditar() dice: Me ha llegado Apellidos: ", $this->apellidos, "<br>";  	// OK
// 		echo "pintaFormEditar() dice: Me ha llegado Telefono: ", $this->telefono, "<br>";  		// OK
// 		echo "pintaFormEditar() dice: Me ha llegado Correo: ", $this->correo, "<br><br>";  		// OK
		
		// FORMULARIO EDITAR
		echo "<form action=\"editar_accion.php\" method=\"post\" class=\"form-horizontal\" >  ";	

		//id (campo oculto, type="hidden")
		echo "<div class=\"form-group\">";
		//echo "<label for=\"id\" class=\"col-sm-2 control-label\">id</label>";
		echo "<div class=\"col-xs-4\"><input type=\"hidden\" class=\"form-control\" id=\"id\" name=\"id\" value=\"", $this->id, "\" ></div></div>";
		 
		echo "<div class=\"form-group\">";
	    echo "<label for=\"nombre\" class=\"col-sm-2 control-label\">Nombre</label>";
		echo "<div class=\"col-xs-4\">";
		echo "<input type=\"text\" class=\"form-control\" id=\"nombre\" name=\"nombre\" value=\"", $this->nombre, "\"></div></div><div class=\"form-group\">";
	    echo "<label for=\"apellidos\" class=\"col-sm-2 control-label\">Apellidos</label><div class=\"col-xs-4\"><input type=\"text\" class=\"form-control\" id=\"apellidos\" name=\"apellidos\" value=\"", $this->apellidos, "\"></div></div><div class=\"form-group\"><label for=\"telefono\" class=\"col-sm-2 control-label\">Telefono</label><div class=\"col-xs-4\"><input type=\"text\" class=\"form-control\" id=\"telefono\" name=\"telefono\" value=\"", $this->telefono, "\"></div></div><div class=\"form-group\"><label for=\"correo\" class=\"col-sm-2 control-label\">Correo</label><div class=\"col-xs-4\"><input type=\"text\" class=\"form-control\" id=\"correo\" name=\"correo\" value=\"", $this->correo, "\"></div></div>	";	    
	    echo "<div class=\"form-group\"><div class=\"col-sm-offset-2 col-sm-10\"><button type=\"submit\" class=\"btn btn-default\">Modificar</button>	";
	    echo "<a href=\"listado.php\"><button type=\"button\" class=\"btn btn-default\" >Cancelar</button></a></div></div></form>";
	}
	//fin function pintaFormEditar
	
	
	


	function modificaContacto($identificador){
	
		//comprobación OK
		echo "modificaContacto() dice: Tengo un obj con id: ", $identificador, "<br>";		 		// OK
		echo "modificaContacto() dice: Tengo un obj con Nombre: ", $this->nombre, "<br>";  			// OK
		echo "modificaContacto() dice: Tengo un obj con Apellidos: ", $this->apellidos, "<br>";  	// OK
		echo "modificaContacto() dice: Tengo un obj con Telefono: ", $this->telefono, "<br>";  		// OK
		echo "modificaContacto() dice: Tengo un obj con Correo: ", $this->correo, "<br><br>";  		// OK


		if ($this->nombre !="" && $this->apellidos != "" && $this->telefono != "" && $this->correo != "") {	// si hay datos en el formulario
			try{
				// crear/conectar a bases de datos
				$conn = new PDO('sqlite:agenda.sqlite');
				
				// preparar sentencia
				// UPDATE contactos SET nombre='PEPE', apellidos= 'DOSANTO',  telefono= '976123456',  correo= 'PEPE@PEPE.COM'  WHERE id = '10';	
				$sentencia = $conn->prepare("UPDATE contactos SET nombre=':nombre', 
															      apellidos= ':apellidos', 
																  telefono= ':telefono', 
																  correo= ':correo'	 
															  WHERE id = ':id' ");
				
				
				// asigno valores
				$id = $identificador;
				$nombre = $this->nombre;
				$apellidos = $this->apellidos;
				$telefono = $this->telefono;
				$correo = $this->correo;
				
				// enlazo parámetros
				$sentencia->bindParam(':id', $id);
				$sentencia->bindParam(':nombre', $nombre);
				$sentencia->bindParam(':apellidos', $apellidos);
				$sentencia->bindParam(':telefono', $telefono);
				$sentencia->bindParam(':correo', $correo);

				// ejecuto sentencia
				$sentencia->execute();
				
				echo "he pasado execute<br>";
		
			} //fin try
			catch(PDOException $e){
				echo $e->getMessage();
			} //fin catch
		
			// cierra conexion
			$conn = null;
				
		}
		//fin if (si se han introducido datos en el formulario)
		
		
		//redirijo a listado
		//redirijo a listado (ha de estar en archivo aparte por el header location)
		include 'redirige_a_listado.php';
	}
	//fin function modificaContacto
	
	
	

	function buscaContacto($cadena, $tipo){
	/*
	 *  $tipo puede ser -1, 1, 2, 3, 4, 5
	 *  <option value="-1" selected="selected">Seleccione una opción</option>
    	<option value="1">id</option>
    	<option value="2">Nombre</option>
    	<option value="3">Apellidos</option>
    	<option value="4">Telefono</option>
    	<option value="5">Correo</option>
	 */
		
		switch ($tipo)
		{
			case "-1":
				$columna = '';
				break;
			case "1":
				$columna = 'id';
				break;
			case "2":
				$columna = 'nombre';
				break;
			case "3":
				$columna = 'apellidos';
				break;
			case "4":
				$columna = 'telefono';
				break;
			case "5":
				$columna = 'correo';
				break;
			default:
				include 'cabecera.php';
				//index contiene menu.php y pie.php
				echo "<div class=\"container\">";
				include 'menu.php';
				echo "<div class=\"jumbotron\"><h2>Has de seleccionar un campo de busqueda !!!!</h2></div>";
				include 'pie.php';
				echo "</div class=\"container\">";
		}
		
		//comprobación OK
// 		echo "buscaContacto() dice: Me ha llegado esta cadena: ", $cadena, "<br>";		// OK
	
		if ($cadena !="" && $columna !="") {	// si hay datos en el formulario
			try{
				// crear/conectar a bases de datos
				$conn = new PDO('sqlite:agenda.sqlite');
				
				// mostrar coincidencias
				$listado = "SELECT * FROM contactos WHERE $columna LIKE '%$cadena%' ";
				$resultado = $conn->query($listado);
				
				//pinta resultados
				include 'cabecera.php';
				echo "<div class=\"container\">";
				include 'menu.php';
				
				echo "<div class=\"jumbotron\"><h2>Coincidencia/s</h2></div>";
				foreach($resultado as $row){
					echo 'id: <strong>', $row['id'], '</strong><br>';
					echo 'Nombre: <strong>', $row['nombre'], '</strong><br>';
					echo 'Apellidos: <strong>', $row['apellidos'], '</strong><br>';
					echo 'Telefono: <strong>', $row['telefono'], '</strong><br>';
					echo 'Correo: <strong>', $row['correo'], '</strong><br>';
					echo '<hr>';
				}
				include 'pie.php';
				echo "</div class=\"container\">";
	
			} //fin try
			catch(PDOException $e){
				echo $e->getMessage();
			} //fin catch
	
			// cierra conexion
			$conn = null;
		}
		//fin if (si se han introducido datos en el formulario)
		
		else {
			include 'cabecera.php';
			//index contiene menu.php y pie.php
			echo "<div class=\"container\">";
			include 'menu.php';
			echo "<div class=\"jumbotron\"><h2>Has de seleccionar un campo de busqueda !!!!</h2></div>";
			include 'pie.php';
			echo "</div class=\"container\">";
		}
		//fin if-else
	}
	//fin function buscaContacto
	
	
} 
//fin class Contacto

?>




<?php
// class Pagina{
// 	public function encabezado($title=""){
// 		echo "<title>Este es el encabezado con " .$title. "</title>";  //el punto concatena (si pongo comillas dobles)
// 	}
	
// 	public function crea_menu($lista){
// 		echo "<ul>";
// 		foreach ($lista as $href=>$texto){
// 			echo "<li><a href=\"$href\">$texto</a></li>";
// 		}
// 		echo "</ul>";
// 	}
// }

?>



<?php 
// 	$p = new Pagina();
// 	$p->encabezado("Hola");
	
	
// 	$opciones = array("inicio"=>"INICIO", "salir"=>"SALIR");
// 	$p->crea_menu($opciones);

?>
