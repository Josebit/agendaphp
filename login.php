<?php 
include "cabecera.php";
?>
  <body>

    <div class="container">

	      <!-- Menu -->
		  <?php 
			include "menu.php";
		  ?>
	      
	      <!-- Main component for a primary marketing message or call to action -->
	      <div class="jumbotron">
	        <h2>Login</h2>
	      </div>
		
	      <!-- Formulario login -->
		  <?php 
			include "login_form.php";
		  ?>
		      

    </div> <!-- /container -->


<!-- Pie (Bootstrap core JavaScript) -->
<?php 
include "pie.php";
?>