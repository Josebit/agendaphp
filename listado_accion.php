<?php
try {
	// crear/conectar a bases de datos
	$conn = new PDO('sqlite:agenda.sqlite');
	
	// select
	$listado = 'SELECT * FROM contactos';
	$resultado = $conn->query($listado);
	
	//para pintar en listado.php
	foreach($resultado as $contacto){
		echo 'id: <strong>', $contacto['id'], '</strong><br>';
		echo 'nombre: <strong>', $contacto['nombre'], '</strong><br>';
		echo 'apellidos: <strong>', $contacto['apellidos'], '</strong><br>';
		echo 'telefono: <strong>', $contacto['telefono'], '</strong><br>';
		echo 'correo: <strong>', $contacto['correo'], '</strong><br>';
		echo '<hr>';
		echo "<a href='./editar.php?id=", $contacto['id'], "' > Editar </a> --- ";
		echo "<a href='./eliminar.php?id=", $contacto['id'], "' > Eliminar </a>";
		echo '<hr>';
	}
		
} //fin try
catch(PDOException $e){
	echo $e->getMessage();
} //fin catch

// cierra conexion
$conn = null;
?>
