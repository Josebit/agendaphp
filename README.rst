Aplicacion AgendaPHP
====================

Sencilla aplicación para gestionar tus contactos. Sólo los usuarios autenticados podrán usar la aplicación.


Instalación
===========

Abre tu navegador y pon la siguiente dirección

::

	https://bitbucket.org/Josebit/agendaphp


Verás un link a descargar.

Te descargas la carpeta, la renombras a 'agenda' y la pones en tu servidor LAMP o XAMP (o el que utilices) le das permisos de lectura y ejecución a 'otros' 

Desde tu navegador:

::

	localhost/agenda



MUY IMPORTANTE:
===============

::
	
	Lo primero que tienes que hacer al accedar a la aplicación 
	por primera vez es hacer click en 'Crear BD inicial', 
	para crear una base de datos y tener unos registros de prueba. 


Esto solo se hace una vez. Y ya podrás utilizar la AgendaPHP.



Uso de la aplicación
====================

Es muy sencillo... Puedes añadir contactos, hacer busquedas por cualquier tipo de dato, borrar contactos o listar tus contactos.


Disfruta !!!

